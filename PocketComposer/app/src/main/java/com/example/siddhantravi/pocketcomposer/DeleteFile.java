package com.example.siddhantravi.pocketcomposer;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

public class DeleteFile extends AppCompatActivity {

    Button yesDeleteButton;
    Button noDeleteButton;

    String songName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_file);

        songName = getIntent().getExtras().getString("SongName").toString();

        yesDeleteButton = (Button)findViewById(R.id.yes_delete);
        noDeleteButton = (Button)findViewById(R.id.no_delete);

        yesDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File fileToBeDeleted = new File(Environment.getExternalStorageDirectory() + File.separator + "MyCompositions" + File.separator + songName);
                fileToBeDeleted.delete();

                Toast.makeText(DeleteFile.this, "Deleting composition...", Toast.LENGTH_LONG).show();

                Intent intent1 = new Intent(DeleteFile.this, MainActivity.class);
                startActivity(intent1);

                finish();

            }
        });

        noDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent2 = new Intent(DeleteFile.this, MainActivity.class);
                startActivity(intent2);

                finish();

            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intentx = new Intent(DeleteFile.this, MainActivity.class);
        startActivity(intentx);
        finish();
    }
}
