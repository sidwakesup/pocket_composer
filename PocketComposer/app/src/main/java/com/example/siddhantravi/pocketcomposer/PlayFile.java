package com.example.siddhantravi.pocketcomposer;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class PlayFile extends AppCompatActivity {


    MediaPlayer mediaPlayer;
    Button buttonPlayPause;
    TextView songDetails;
    TextView textState;

    String songName;

    private int stateMediaPlayer;
    private final int stateMP_Error = 0;
    private final int stateMP_NotStarter = 1;
    private final int stateMP_Playing = 2;
    private final int stateMP_Pausing = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_file);

        songName = getIntent().getExtras().getString("SongName").toString();

        buttonPlayPause = (Button)findViewById(R.id.playpause);
        songDetails = (TextView)findViewById(R.id.song_details);
        textState = (TextView)findViewById(R.id.state);

        songDetails.setText(songName);

        buttonPlayPause.setOnClickListener(buttonPlayPauseOnClickListener);

        initMediaPlayer();


    }

    private void initMediaPlayer() {
        String PATH_TO_FILE = Environment.getExternalStorageDirectory() + File.separator + "MyCompositions" + File.separator + songName;
        mediaPlayer = new MediaPlayer();

        try {
            mediaPlayer.setDataSource(PATH_TO_FILE);
            mediaPlayer.prepare();
            stateMediaPlayer = stateMP_NotStarter;
            textState.setText("-IDLE-");

        }catch(IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(PlayFile.this, e.toString(), Toast.LENGTH_LONG).show();
            stateMediaPlayer = stateMP_Error;
            textState.setText("-ERROR-");
        }catch(IllegalStateException e) {
            e.printStackTrace();
            Toast.makeText(PlayFile.this, e.toString(), Toast.LENGTH_LONG).show();
            stateMediaPlayer = stateMP_Error;
            textState.setText("-ERROR-");
        }catch(IOException e) {
            e.printStackTrace();
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
            stateMediaPlayer = stateMP_Error;
            textState.setText("-ERROR-");
        }

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                buttonPlayPause.setBackgroundResource(R.drawable.play_icon);
                textState.setText("-IDLE-");
            }
        });
    }

    Button.OnClickListener buttonPlayPauseOnClickListener = new Button.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch(stateMediaPlayer) {
                case stateMP_Error:
                    break;
                case stateMP_NotStarter:
                    mediaPlayer.start();
                    buttonPlayPause.setBackgroundResource(R.drawable.pause_icon);
                    textState.setText("-PLAYING-");
                    stateMediaPlayer = stateMP_Playing;
                    break;
                case stateMP_Playing:
                    mediaPlayer.pause();
                    buttonPlayPause.setBackgroundResource(R.drawable.play_icon);
                    textState.setText("-PAUSED-");
                    stateMediaPlayer = stateMP_Pausing;
                    break;
                case stateMP_Pausing:
                    mediaPlayer.start();
                    buttonPlayPause.setBackgroundResource(R.drawable.pause_icon);
                    textState.setText("-PLAYING-");
                    stateMediaPlayer = stateMP_Playing;
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        mediaPlayer.stop();
        mediaPlayer.release();

        Intent intentx = new Intent(PlayFile.this, MainActivity.class);
        startActivity(intentx);

        finish();
    }

}
