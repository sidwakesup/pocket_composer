package com.example.siddhantravi.pocketcomposer;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;

public class RenameFile extends AppCompatActivity {

    String songName;
    String newSongName;

    EditText newName;
    Button renameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rename_file);

        songName = getIntent().getExtras().getString("SongName").toString();



        renameButton = (Button)findViewById(R.id.submit_button);
        renameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newName = (EditText)findViewById(R.id.new_name);
                newSongName = newName.getText().toString();

                if(newSongName.isEmpty() || !newSongName.contains(".3gp")) {
                    Toast.makeText(RenameFile.this, "Please enter a valid file name", Toast.LENGTH_LONG).show();
                }
                else {

                    File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "MyCompositions");
                    File from = new File(directory, songName);
                    File to = new File(directory, newSongName);
                    from.renameTo(to);

                    Toast.makeText(RenameFile.this, "Renaming composition...", Toast.LENGTH_LONG).show();

                    Intent intent1 = new Intent(RenameFile.this, MainActivity.class);
                    startActivity(intent1);

                    finish();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intentx = new Intent(RenameFile.this, MainActivity.class);
        startActivity(intentx);
        finish();
    }
}
