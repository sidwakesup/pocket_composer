package com.example.siddhantravi.pocketcomposer;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private File root;
    private ArrayList<String> fileList = new ArrayList<String>();
    private RelativeLayout view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("My Compositions");

        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "MyCompositions");
        boolean success = true;

        if(!folder.exists()) {
            success = folder.mkdir();
        }
        if(success) {
            Log.d("Music", "executing");

            root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyCompositions");
            getfile(root);

            if(fileList.isEmpty()) {
                TextView emptyDirectory = new TextView(MainActivity.this);
                emptyDirectory.setText("No compositions.");
                emptyDirectory.setTextColor(Color.WHITE);
                emptyDirectory.setTextSize(30);


            }
            else {
                ArrayAdapter<String> musicFilesAdapter = new ArrayAdapter<String>(MainActivity.this, R.layout.list_music_files, fileList);
                Log.d("Music", "arrayadapter");
                ListView listView = (ListView) findViewById(R.id.list_of_music_files);
                listView.setAdapter(musicFilesAdapter);
                Log.d("Music", "listview");

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                        //Toast.makeText(MainActivity.this, "You clicked: " + parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();

                        PopupMenu popup = new PopupMenu(MainActivity.this, view);
                        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                        Log.d("Music", "popup menu created");
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {

                                //Toast.makeText(MainActivity.this, "You want to " + item.getTitle() + " file", Toast.LENGTH_SHORT).show();

                                switch (item.getTitle().toString()) {
                                    case "Play":
                                        Intent intent1 = new Intent(MainActivity.this, PlayFile.class);
                                        intent1.putExtra("SongName", parent.getItemAtPosition(position).toString());
                                        startActivity(intent1);
                                        finish();
                                        break;
                                    case "Rename":
                                        Intent intent2 = new Intent(MainActivity.this, RenameFile.class);
                                        intent2.putExtra("SongName", parent.getItemAtPosition(position).toString());
                                        startActivity(intent2);
                                        finish();
                                        break;
                                    case "Delete":
                                        Intent intent3 = new Intent(MainActivity.this, DeleteFile.class);
                                        intent3.putExtra("SongName", parent.getItemAtPosition(position).toString());
                                        startActivity(intent3);
                                        finish();
                                        break;
                                    default:
                                        Toast.makeText(MainActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                                }

                                return true;
                            }
                        });

                        popup.show();
                    }
                });

            }


        }
        else {

           TextView textView = new TextView(this);
            textView.setText("Directory not found.");

            view.addView(textView);

        }



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToNewCompositionMenu = new Intent(MainActivity.this, EDMComposer.class);
                startActivity(goToNewCompositionMenu);
            }
        });

    }

    public ArrayList<String> getfile(File dir) {

        String listFile[] = dir.list();

        if(listFile != null && listFile.length > 0) {
            for(int i = 0; i < listFile.length; i++) {
                Log.d("Music", "fileno: "+listFile[i]);
                    if(listFile[i].endsWith(".mp3") || listFile[i].endsWith(".3gp") || listFile[i].contains(".wav")) {
                        fileList.add(listFile[i]);
                        Log.d("Music", listFile[i] + " added");
                    }

            }
        }

        return fileList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }



}
