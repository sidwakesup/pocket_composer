package com.example.siddhantravi.pocketcomposer;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.SoundPool;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

public class EDMComposer extends AppCompatActivity implements View.OnTouchListener {

    private static final int numberOfWhiteKeys = 14;
    private static final int numberOfBlackKeys = 10;
    private static final int numberOfKeys = numberOfWhiteKeys + numberOfBlackKeys;
    private Region[] keyboard = new Region[numberOfKeys];
    private MediaPlayer[] key = new MediaPlayer[numberOfKeys];
    int screenWidth;
    int screenHeight;
    private int[] activePointers = new int[numberOfKeys];
    private Drawable drawableWhite;
    private Drawable drawableBlack;
    private Drawable drawableWhitePressed;
    private Drawable drawableBlackPressed;
    private Timer timer;
    private Bitmap bitmap_keyboard;
    private ImageView iv;
    private boolean[] lastPlayingNotes;
    //boolean flag = false;

    public final int MAX_NUMBER_OF_STREAMS_FOR_BACKGROUND = 15;
    public final int SOURCE_QUALITY = 0;

    int current_playing_strings_ID = 0;
    int currently_playing_beat_ID = 0;

    int beat_1_playing = 0;
    int beat_2_playing = 0;
    int beat_3_playing = 0;

    int tempo = 0;

    public int strings_playing[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    SoundPool edmCompositionSoundPoolBackground = new SoundPool(MAX_NUMBER_OF_STREAMS_FOR_BACKGROUND, AudioManager.STREAM_MUSIC, SOURCE_QUALITY);


    Button record;

    int isRecording = 0;

    private MediaRecorder myAudioRecorder;
    private String outputFile = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_edm_composer);


        TypedArray notes = this.getResources().obtainTypedArray(R.array.edmnotes);

        for(int i = 0;i < notes.length(); i++) {
            int k = notes.getResourceId(i, -1);
            if(k!=-1) {
                key[i] = MediaPlayer.create(this, k);

            }
            else {
                key[i] = null;
            }
        }

        Resources res = this.getResources();
        drawableWhite = res.getDrawable(R.drawable.white);
        drawableBlack = res.getDrawable(R.drawable.black);
        drawableWhitePressed = res.getDrawable(R.drawable.white_pressed);
        drawableBlackPressed = res.getDrawable(R.drawable.black_pressed);

        Display disp = ((WindowManager) this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        screenWidth = disp.getWidth();
        screenHeight = disp.getHeight();

        this.makeRegions();

        for(int i = 0; i < numberOfKeys; i++) {
            activePointers[i] = -1;
        }

        iv = (ImageView)findViewById(R.id.edm_keyboard);
        iv.setOnTouchListener(this);

        final Spinner tempoSetting = (Spinner)findViewById(R.id.tempo_settings);
        String[] items = new String[]{"60", "70", "80", "90", "100", "110", "120", "130", "140", "150", "160", "170", "180", "190", "200"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        tempoSetting.setAdapter(adapter);

        tempoSetting.setSelection(7);

        tempoSetting.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tempo = Integer.parseInt(tempoSetting.getSelectedItem().toString());
                Log.d("Music", "Current tempo: " + tempo);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        record = (Button)findViewById(R.id.edm_record);

        outputFile = Environment.getExternalStorageDirectory() + File.separator + "MyCompositions" + File.separator + "recording0.3gp";

        myAudioRecorder = new MediaRecorder();
        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myAudioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        myAudioRecorder.setOutputFile(outputFile);

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isRecording==0) {
                    try {
                        myAudioRecorder.prepare();
                        myAudioRecorder.start();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                    catch(IOException e) {
                        e.printStackTrace();
                    }
                    isRecording = 1;
                    Toast.makeText(EDMComposer.this, "Recording started", Toast.LENGTH_SHORT).show();
                    Log.d("record", "recording started");
                }
                else if(isRecording==1) {
                    myAudioRecorder.stop();
                    myAudioRecorder.release();
                    myAudioRecorder = null;
                    isRecording = 0;
                    Toast.makeText(EDMComposer.this, "Audio recorded successfully", Toast.LENGTH_LONG).show();
                    Log.d("record", "recording finished");
                    Intent renameFile = new Intent(EDMComposer.this, RenameFile.class);
                    renameFile.putExtra("SongName", "recording0.3gp");
                    startActivity(renameFile);
                }
            }
        });


    }



    private void makeRegions() {
        int keyWidth;
        int keyHeight;
        int blackKeyWidth;
        int blackKeyHeight;

        keyWidth = (int) (screenWidth/numberOfWhiteKeys);
        keyHeight = (int) ((screenHeight/2)*0.8);
        blackKeyWidth = (int) (keyWidth*0.6);
        blackKeyHeight = (int) ((screenHeight/2)*0.5);

        Path[] path = new Path[4];
        path[0] = new Path();
        path[1] = new Path();
        path[2] = new Path();
        path[3] = new Path();

        path[0].lineTo(0, keyHeight);
        path[0].lineTo(keyWidth, keyHeight);
        path[0].lineTo(keyWidth, blackKeyWidth);
        path[0].lineTo(keyWidth - (blackKeyWidth/2), blackKeyHeight);
        path[0].lineTo(keyWidth - (blackKeyWidth / 2), 0);
        path[0].close();

        path[1].moveTo(blackKeyWidth / 2, 0);
        path[1].lineTo(blackKeyWidth / 2, blackKeyHeight);
        path[1].lineTo(0, blackKeyHeight);
        path[1].lineTo(0, keyHeight);
        path[1].lineTo(keyWidth, keyHeight);
        path[1].lineTo(keyWidth, blackKeyHeight);
        path[1].lineTo(keyWidth - (blackKeyWidth / 2), blackKeyHeight);
        path[1].lineTo(keyWidth - (blackKeyWidth / 2), 0);
        path[1].close();

        path[2].moveTo(blackKeyWidth / 2, 0);
        path[2].lineTo(blackKeyWidth / 2, blackKeyHeight);
        path[2].lineTo(0, blackKeyHeight);
        path[2].lineTo(0, keyHeight);
        path[2].lineTo(keyWidth, keyHeight);
        path[2].lineTo(keyWidth, 0);
        path[2].close();

        path[3].addRect(0, 0, blackKeyWidth, blackKeyHeight, Path.Direction.CCW);

        Region region = new Region(0, 0, screenWidth, (screenHeight/2));
        int kt[] = new int[] {0, 1, 2, 0, 1, 1, 2, 0, 1, 2, 0, 1, 1, 2, 3, 3, -1, 3, 3, 3, -1, 3, 3, -1, 3, 3, 3};

        for(int i = 0; i < numberOfWhiteKeys; i++) {
            keyboard[i] = new Region();
            Path pathtmp = new Path();
            pathtmp.addPath(path[kt[i]], i * keyWidth, 0);
            keyboard[i].setPath(pathtmp, region);
        }

        int j = numberOfWhiteKeys;

        for(int i = numberOfWhiteKeys; i < kt.length; i++) {
            if(kt[i]!=-1) {
                keyboard[j] = new Region();
                Path pathtmp = new Path();
                pathtmp.addPath(path[kt[i]], (i-numberOfWhiteKeys+1)*keyWidth-(blackKeyWidth/2), 0);
                keyboard[j].setPath(pathtmp, region);
                j = j + 1;
            }
        }

    }

    private Bitmap drawKeys() {
        Bitmap bm = Bitmap.createBitmap(screenWidth, (screenHeight/2), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);

        for(int i = 0; i < numberOfWhiteKeys; i++) {
            if(key[i].isPlaying()) {
                drawableWhitePressed.setBounds(keyboard[i].getBounds());
                drawableWhitePressed.draw(canvas);
            }
            else {
                drawableWhite.setBounds(keyboard[i].getBounds());
                drawableWhite.draw(canvas);
            }
        }

        for(int i = numberOfWhiteKeys; i < numberOfKeys; i++) {
            if(key[i].isPlaying()) {
                drawableBlackPressed.setBounds(keyboard[i].getBounds());
                drawableBlackPressed.draw(canvas);
            }
            else {
                drawableBlack.setBounds(keyboard[i].getBounds());
                drawableBlack.draw(canvas);
            }
        }

        return bm;
    }


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        int pointerIndex = event.getActionIndex();
        float x = event.getX(pointerIndex);
        float y = event.getY(pointerIndex);

        for(int j = 0; j < numberOfKeys; j++) {
            if(keyboard[j].contains((int)x, (int)y)) {
                Log.d("notes", String.valueOf(j));
                switch(event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_POINTER_DOWN:
                        playNote(key[j]);
                        activePointers[pointerIndex] = j;
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_POINTER_UP:
                        stopNote(key[j]);
                        activePointers[pointerIndex] = -1;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if(activePointers[pointerIndex] != j) {
                            if(activePointers[pointerIndex] != -1) {
                                stopNote(key[activePointers[pointerIndex]]);
                            }
                            playNote(key[j]);
                            activePointers[pointerIndex] = j;
                        }
                        break;
                }
            }
        }

        return true;
    }


    private void playNote(MediaPlayer mp) {
      //  if(flag==true) {

                    mp.seekTo(0);
                    mp.start();

     //   }
        //mp.setLooping(true);
    }

    private void stopNote(MediaPlayer mp) {
            mp.pause();
            mp.seekTo(0);
    }

    @Override
    protected void onResume() {
        super.onResume();

        timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {

                boolean[] playingNotes = new boolean[numberOfKeys];
                for (int i = 0; i < playingNotes.length; i++) {
                    playingNotes[i] = key[i].isPlaying();
                }

                if (!Arrays.equals(playingNotes, lastPlayingNotes)) {
                    bitmap_keyboard = drawKeys();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            iv.setImageBitmap(bitmap_keyboard);
                        }
                    });
                }

                lastPlayingNotes = playingNotes;

            }
        }, 0, 100);
    }

    public void playChords(View view) throws InterruptedException {
        switch(view.getId()) {
            case R.id.edm_c:
                int C_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.c_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=0 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button cs = (Button)findViewById(R.id.edm_c_sharp);
                        cs.setBackgroundResource(android.R.drawable.btn_default);
                        Button d = (Button)findViewById(R.id.edm_d);
                        d.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button e = (Button)findViewById(R.id.edm_e);
                        e.setBackgroundResource(android.R.drawable.btn_default);
                        Button f = (Button)findViewById(R.id.edm_f);
                        f.setBackgroundResource(android.R.drawable.btn_default);
                        Button fs = (Button)findViewById(R.id.edm_f_sharp);
                        fs.setBackgroundResource(android.R.drawable.btn_default);
                        Button g = (Button)findViewById(R.id.edm_g);
                        g.setBackgroundResource(android.R.drawable.btn_default);
                        Button gs = (Button)findViewById(R.id.edm_g_sharp);
                        gs.setBackgroundResource(android.R.drawable.btn_default);
                        Button a = (Button)findViewById(R.id.edm_a);
                        a.setBackgroundResource(android.R.drawable.btn_default);
                        Button as = (Button)findViewById(R.id.edm_a_sharp);
                        as.setBackgroundResource(android.R.drawable.btn_default);
                        Button b = (Button)findViewById(R.id.edm_b);
                        b.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[0]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(C_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[0] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[0] = 0;
                }
                break;


            case R.id.edm_c_sharp:
                int C_Sharp_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.c_sharp_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=1 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button c = (Button)findViewById(R.id.edm_c);
                        c.setBackgroundResource(android.R.drawable.btn_default);
                        Button d = (Button)findViewById(R.id.edm_d);
                        d.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button e = (Button)findViewById(R.id.edm_e);
                        e.setBackgroundResource(android.R.drawable.btn_default);
                        Button f = (Button)findViewById(R.id.edm_f);
                        f.setBackgroundResource(android.R.drawable.btn_default);
                        Button fs = (Button)findViewById(R.id.edm_f_sharp);
                        fs.setBackgroundResource(android.R.drawable.btn_default);
                        Button g = (Button)findViewById(R.id.edm_g);
                        g.setBackgroundResource(android.R.drawable.btn_default);
                        Button gs = (Button)findViewById(R.id.edm_g_sharp);
                        gs.setBackgroundResource(android.R.drawable.btn_default);
                        Button a = (Button)findViewById(R.id.edm_a);
                        a.setBackgroundResource(android.R.drawable.btn_default);
                        Button as = (Button)findViewById(R.id.edm_a_sharp);
                        as.setBackgroundResource(android.R.drawable.btn_default);
                        Button b = (Button)findViewById(R.id.edm_b);
                        b.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[1]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(C_Sharp_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[1] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[1] = 0;
                }
                break;


            case R.id.edm_d:
                int D_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.d_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=2 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button c = (Button)findViewById(R.id.edm_c);
                        c.setBackgroundResource(android.R.drawable.btn_default);
                        Button cs = (Button)findViewById(R.id.edm_c_sharp);
                        cs.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button e = (Button)findViewById(R.id.edm_e);
                        e.setBackgroundResource(android.R.drawable.btn_default);
                        Button f = (Button)findViewById(R.id.edm_f);
                        f.setBackgroundResource(android.R.drawable.btn_default);
                        Button fs = (Button)findViewById(R.id.edm_f_sharp);
                        fs.setBackgroundResource(android.R.drawable.btn_default);
                        Button g = (Button)findViewById(R.id.edm_g);
                        g.setBackgroundResource(android.R.drawable.btn_default);
                        Button gs = (Button)findViewById(R.id.edm_g_sharp);
                        gs.setBackgroundResource(android.R.drawable.btn_default);
                        Button a = (Button)findViewById(R.id.edm_a);
                        a.setBackgroundResource(android.R.drawable.btn_default);
                        Button as = (Button)findViewById(R.id.edm_a_sharp);
                        as.setBackgroundResource(android.R.drawable.btn_default);
                        Button b = (Button)findViewById(R.id.edm_b);
                        b.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[2]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(D_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[2] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[2] = 0;
                }
                break;


            case R.id.edm_d_sharp:
                int D_Sharp_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.d_sharp_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=3 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button c = (Button)findViewById(R.id.edm_c);
                        c.setBackgroundResource(android.R.drawable.btn_default);
                        Button d = (Button)findViewById(R.id.edm_d);
                        d.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button e = (Button)findViewById(R.id.edm_e);
                        e.setBackgroundResource(android.R.drawable.btn_default);
                        Button f = (Button)findViewById(R.id.edm_f);
                        f.setBackgroundResource(android.R.drawable.btn_default);
                        Button fs = (Button)findViewById(R.id.edm_f_sharp);
                        fs.setBackgroundResource(android.R.drawable.btn_default);
                        Button g = (Button)findViewById(R.id.edm_g);
                        g.setBackgroundResource(android.R.drawable.btn_default);
                        Button gs = (Button)findViewById(R.id.edm_g_sharp);
                        gs.setBackgroundResource(android.R.drawable.btn_default);
                        Button a = (Button)findViewById(R.id.edm_a);
                        a.setBackgroundResource(android.R.drawable.btn_default);
                        Button as = (Button)findViewById(R.id.edm_a_sharp);
                        as.setBackgroundResource(android.R.drawable.btn_default);
                        Button b = (Button)findViewById(R.id.edm_b);
                        b.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[3]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(D_Sharp_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[3] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[3] = 0;
                }
                break;


            case R.id.edm_e:
                int E_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.e_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=4 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button c = (Button)findViewById(R.id.edm_c);
                        c.setBackgroundResource(android.R.drawable.btn_default);
                        Button cs = (Button)findViewById(R.id.edm_c_sharp);
                        cs.setBackgroundResource(android.R.drawable.btn_default);
                        Button d = (Button)findViewById(R.id.edm_d_sharp);
                        d.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button f = (Button)findViewById(R.id.edm_f);
                        f.setBackgroundResource(android.R.drawable.btn_default);
                        Button fs = (Button)findViewById(R.id.edm_f_sharp);
                        fs.setBackgroundResource(android.R.drawable.btn_default);
                        Button g = (Button)findViewById(R.id.edm_g);
                        g.setBackgroundResource(android.R.drawable.btn_default);
                        Button gs = (Button)findViewById(R.id.edm_g_sharp);
                        gs.setBackgroundResource(android.R.drawable.btn_default);
                        Button a = (Button)findViewById(R.id.edm_a);
                        a.setBackgroundResource(android.R.drawable.btn_default);
                        Button as = (Button)findViewById(R.id.edm_a_sharp);
                        as.setBackgroundResource(android.R.drawable.btn_default);
                        Button b = (Button)findViewById(R.id.edm_b);
                        b.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[4]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(E_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[4] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[4] = 0;
                }
                break;

            case R.id.edm_f:
                int F_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.f_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=5 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button c = (Button)findViewById(R.id.edm_c);
                        c.setBackgroundResource(android.R.drawable.btn_default);
                        Button cs = (Button)findViewById(R.id.edm_c_sharp);
                        cs.setBackgroundResource(android.R.drawable.btn_default);
                        Button d = (Button)findViewById(R.id.edm_d_sharp);
                        d.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button e = (Button)findViewById(R.id.edm_e);
                        e.setBackgroundResource(android.R.drawable.btn_default);
                        Button fs = (Button)findViewById(R.id.edm_f_sharp);
                        fs.setBackgroundResource(android.R.drawable.btn_default);
                        Button g = (Button)findViewById(R.id.edm_g);
                        g.setBackgroundResource(android.R.drawable.btn_default);
                        Button gs = (Button)findViewById(R.id.edm_g_sharp);
                        gs.setBackgroundResource(android.R.drawable.btn_default);
                        Button a = (Button)findViewById(R.id.edm_a);
                        a.setBackgroundResource(android.R.drawable.btn_default);
                        Button as = (Button)findViewById(R.id.edm_a_sharp);
                        as.setBackgroundResource(android.R.drawable.btn_default);
                        Button b = (Button)findViewById(R.id.edm_b);
                        b.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[5]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(F_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[5] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[5] = 0;
                }
                break;

            case R.id.edm_f_sharp:
                int F_Sharp_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.f_sharp_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=6 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button c = (Button)findViewById(R.id.edm_c);
                        c.setBackgroundResource(android.R.drawable.btn_default);
                        Button cs = (Button)findViewById(R.id.edm_c_sharp);
                        cs.setBackgroundResource(android.R.drawable.btn_default);
                        Button d = (Button)findViewById(R.id.edm_d_sharp);
                        d.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button e = (Button)findViewById(R.id.edm_e);
                        e.setBackgroundResource(android.R.drawable.btn_default);
                        Button f = (Button)findViewById(R.id.edm_f);
                        f.setBackgroundResource(android.R.drawable.btn_default);
                        Button g = (Button)findViewById(R.id.edm_g);
                        g.setBackgroundResource(android.R.drawable.btn_default);
                        Button gs = (Button)findViewById(R.id.edm_g_sharp);
                        gs.setBackgroundResource(android.R.drawable.btn_default);
                        Button a = (Button)findViewById(R.id.edm_a);
                        a.setBackgroundResource(android.R.drawable.btn_default);
                        Button as = (Button)findViewById(R.id.edm_a_sharp);
                        as.setBackgroundResource(android.R.drawable.btn_default);
                        Button b = (Button)findViewById(R.id.edm_b);
                        b.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[6]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(F_Sharp_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[6] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[6] = 0;
                }
                break;

            case R.id.edm_g:
                int G_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.g_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=7 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button c = (Button)findViewById(R.id.edm_c);
                        c.setBackgroundResource(android.R.drawable.btn_default);
                        Button cs = (Button)findViewById(R.id.edm_c_sharp);
                        cs.setBackgroundResource(android.R.drawable.btn_default);
                        Button d = (Button)findViewById(R.id.edm_d_sharp);
                        d.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button e = (Button)findViewById(R.id.edm_e);
                        e.setBackgroundResource(android.R.drawable.btn_default);
                        Button f = (Button)findViewById(R.id.edm_f);
                        f.setBackgroundResource(android.R.drawable.btn_default);
                        Button fs = (Button)findViewById(R.id.edm_f_sharp);
                        fs.setBackgroundResource(android.R.drawable.btn_default);
                        Button gs = (Button)findViewById(R.id.edm_g_sharp);
                        gs.setBackgroundResource(android.R.drawable.btn_default);
                        Button a = (Button)findViewById(R.id.edm_a);
                        a.setBackgroundResource(android.R.drawable.btn_default);
                        Button as = (Button)findViewById(R.id.edm_a_sharp);
                        as.setBackgroundResource(android.R.drawable.btn_default);
                        Button b = (Button)findViewById(R.id.edm_b);
                        b.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[7]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(G_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[7] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[7] = 0;
                }
                break;

            case R.id.edm_g_sharp:
                int G_Sharp_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.g_sharp_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=8 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button c = (Button)findViewById(R.id.edm_c);
                        c.setBackgroundResource(android.R.drawable.btn_default);
                        Button cs = (Button)findViewById(R.id.edm_c_sharp);
                        cs.setBackgroundResource(android.R.drawable.btn_default);
                        Button d = (Button)findViewById(R.id.edm_d_sharp);
                        d.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button e = (Button)findViewById(R.id.edm_e);
                        e.setBackgroundResource(android.R.drawable.btn_default);
                        Button f = (Button)findViewById(R.id.edm_f);
                        f.setBackgroundResource(android.R.drawable.btn_default);
                        Button fs = (Button)findViewById(R.id.edm_f_sharp);
                        fs.setBackgroundResource(android.R.drawable.btn_default);
                        Button g = (Button)findViewById(R.id.edm_g);
                        g.setBackgroundResource(android.R.drawable.btn_default);
                        Button a = (Button)findViewById(R.id.edm_a);
                        a.setBackgroundResource(android.R.drawable.btn_default);
                        Button as = (Button)findViewById(R.id.edm_a_sharp);
                        as.setBackgroundResource(android.R.drawable.btn_default);
                        Button b = (Button)findViewById(R.id.edm_b);
                        b.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[8]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(G_Sharp_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[8] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[8] = 0;
                }
                break;

            case R.id.edm_a:
                int A_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.a_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=9 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button c = (Button)findViewById(R.id.edm_c);
                        c.setBackgroundResource(android.R.drawable.btn_default);
                        Button cs = (Button)findViewById(R.id.edm_c_sharp);
                        cs.setBackgroundResource(android.R.drawable.btn_default);
                        Button d = (Button)findViewById(R.id.edm_d_sharp);
                        d.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button e = (Button)findViewById(R.id.edm_e);
                        e.setBackgroundResource(android.R.drawable.btn_default);
                        Button f = (Button)findViewById(R.id.edm_f);
                        f.setBackgroundResource(android.R.drawable.btn_default);
                        Button fs = (Button)findViewById(R.id.edm_f_sharp);
                        fs.setBackgroundResource(android.R.drawable.btn_default);
                        Button g = (Button)findViewById(R.id.edm_g);
                        g.setBackgroundResource(android.R.drawable.btn_default);
                        Button gs = (Button)findViewById(R.id.edm_g_sharp);
                        gs.setBackgroundResource(android.R.drawable.btn_default);
                        Button as = (Button)findViewById(R.id.edm_a_sharp);
                        as.setBackgroundResource(android.R.drawable.btn_default);
                        Button b = (Button)findViewById(R.id.edm_b);
                        b.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[9]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(A_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[9] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[9] = 0;
                }
                break;

            case R.id.edm_a_sharp:
                int A_Sharp_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.a_sharp_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=10 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button c = (Button)findViewById(R.id.edm_c);
                        c.setBackgroundResource(android.R.drawable.btn_default);
                        Button cs = (Button)findViewById(R.id.edm_c_sharp);
                        cs.setBackgroundResource(android.R.drawable.btn_default);
                        Button d = (Button)findViewById(R.id.edm_d_sharp);
                        d.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button e = (Button)findViewById(R.id.edm_e);
                        e.setBackgroundResource(android.R.drawable.btn_default);
                        Button f = (Button)findViewById(R.id.edm_f);
                        f.setBackgroundResource(android.R.drawable.btn_default);
                        Button fs = (Button)findViewById(R.id.edm_f_sharp);
                        fs.setBackgroundResource(android.R.drawable.btn_default);
                        Button g = (Button)findViewById(R.id.edm_g);
                        g.setBackgroundResource(android.R.drawable.btn_default);
                        Button gs = (Button)findViewById(R.id.edm_g_sharp);
                        gs.setBackgroundResource(android.R.drawable.btn_default);
                        Button a = (Button)findViewById(R.id.edm_a);
                        a.setBackgroundResource(android.R.drawable.btn_default);
                        Button b = (Button)findViewById(R.id.edm_b);
                        b.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[10]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(A_Sharp_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[10] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[10] = 0;
                }
                break;

            case R.id.edm_b:
                int B_Strings = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.b_strings_background_edm_compose, 1);
                Thread.sleep(250);

                for(int i = 0; i < 12; i++) {
                    if(i!=11 && strings_playing[i]!=0) {
                        edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);

                        Button c = (Button)findViewById(R.id.edm_c);
                        c.setBackgroundResource(android.R.drawable.btn_default);
                        Button cs = (Button)findViewById(R.id.edm_c_sharp);
                        cs.setBackgroundResource(android.R.drawable.btn_default);
                        Button d = (Button)findViewById(R.id.edm_d_sharp);
                        d.setBackgroundResource(android.R.drawable.btn_default);
                        Button ds = (Button)findViewById(R.id.edm_d_sharp);
                        ds.setBackgroundResource(android.R.drawable.btn_default);
                        Button e = (Button)findViewById(R.id.edm_e);
                        e.setBackgroundResource(android.R.drawable.btn_default);
                        Button f = (Button)findViewById(R.id.edm_f);
                        f.setBackgroundResource(android.R.drawable.btn_default);
                        Button fs = (Button)findViewById(R.id.edm_f_sharp);
                        fs.setBackgroundResource(android.R.drawable.btn_default);
                        Button g = (Button)findViewById(R.id.edm_g);
                        g.setBackgroundResource(android.R.drawable.btn_default);
                        Button gs = (Button)findViewById(R.id.edm_g_sharp);
                        gs.setBackgroundResource(android.R.drawable.btn_default);
                        Button a = (Button)findViewById(R.id.edm_a);
                        a.setBackgroundResource(android.R.drawable.btn_default);
                        Button as = (Button)findViewById(R.id.edm_a_sharp);
                        as.setBackgroundResource(android.R.drawable.btn_default);
                        strings_playing[i] = 0;
                    }
                }

                if(strings_playing[11]==0) {
                    current_playing_strings_ID = edmCompositionSoundPoolBackground.play(B_Strings, 0.8f, 0.8f, 1, -1, 1.0f);
                    strings_playing[11] = 1;
                    view.setBackgroundColor(Color.BLACK);
                }
                else {
                    edmCompositionSoundPoolBackground.stop(current_playing_strings_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    strings_playing[11] = 0;
                }
                break;



        }
    }

    public void playBeats(View view) throws InterruptedException {
        switch(view.getId()) {
            case R.id.edm_beat_1:
                int beat_1_100 = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.beat_1_100_edm_compose, 1);

                Thread.sleep(500);

                if(beat_2_playing!=0 || beat_3_playing!=0) {
                    edmCompositionSoundPoolBackground.stop(currently_playing_beat_ID);
                    Button beat_2 = (Button) findViewById(R.id.edm_beat_2);
                    beat_2.setBackgroundResource(android.R.drawable.btn_default);
                    Button beat_3 = (Button) findViewById(R.id.edm_beat_3);
                    beat_3.setBackgroundResource(android.R.drawable.btn_default);
                    beat_2_playing = 0;
                    beat_3_playing = 0;

                }

                if(beat_1_playing==0) {
                    switch(tempo) {
                        case 60:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 0.6f);
                            break;
                        case 70:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 0.7f);
                            break;
                        case 80:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 0.8f);
                            break;
                        case 90:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 0.9f);
                            break;
                        case 100:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 1.0f);
                            break;
                        case 110:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 1.1f);
                            break;
                        case 120:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 1.2f);
                            break;
                        case 130:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 1.3f);
                            break;
                        case 140:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 1.4f);
                            break;
                        case 150:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 1.5f);
                            break;
                        case 160:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 1.6f);
                            break;
                        case 170:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 1.7f);
                            break;
                        case 180:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 1.8f);
                            break;
                        case 190:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 1.9f);
                            break;
                        case 200:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_1_100, 0.7f, 0.7f, 1, -1, 2.0f);
                            break;
                    }

                    view.setBackgroundColor(Color.BLACK);
                    beat_1_playing = 1;


                }
                else {
                    edmCompositionSoundPoolBackground.stop(currently_playing_beat_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    beat_1_playing = 0;
                }
                break;

            case R.id.edm_beat_2:
                int beat_2_100 = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.beat_2_100_edm_compose, 1);

                Thread.sleep(500);

                if(beat_1_playing!=0 || beat_3_playing!=0) {
                    edmCompositionSoundPoolBackground.stop(currently_playing_beat_ID);
                    Button beat_1 = (Button) findViewById(R.id.edm_beat_1);
                    beat_1.setBackgroundResource(android.R.drawable.btn_default);
                    Button beat_3 = (Button) findViewById(R.id.edm_beat_3);
                    beat_3.setBackgroundResource(android.R.drawable.btn_default);
                    beat_1_playing = 0;
                    beat_3_playing = 0;

                }

                if(beat_2_playing==0) {
                    switch(tempo) {
                        case 60:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 0.6f);
                            break;
                        case 70:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 0.7f);
                            break;
                        case 80:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 0.8f);
                            break;
                        case 90:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 0.9f);
                            break;
                        case 100:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 1.0f);
                            break;
                        case 110:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 1.1f);
                            break;
                        case 120:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 1.2f);
                            break;
                        case 130:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 1.3f);
                            break;
                        case 140:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 1.4f);
                            break;
                        case 150:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 1.5f);
                            break;
                        case 160:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 1.6f);
                            break;
                        case 170:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 1.7f);
                            break;
                        case 180:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 1.8f);
                            break;
                        case 190:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 1.9f);
                            break;
                        case 200:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_2_100, 0.7f, 0.7f, 1, -1, 2.0f);
                            break;
                    }

                    view.setBackgroundColor(Color.BLACK);
                    beat_2_playing = 1;


                }
                else {
                    edmCompositionSoundPoolBackground.stop(currently_playing_beat_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    beat_2_playing = 0;
                }
                break;

            case R.id.edm_beat_3:
                int beat_3_100 = edmCompositionSoundPoolBackground.load(EDMComposer.this, R.raw.beat_3_100_edm_compose, 1);
                Thread.sleep(500);

                if(beat_1_playing!=0 || beat_2_playing!=0) {
                    edmCompositionSoundPoolBackground.stop(currently_playing_beat_ID);
                    Button beat_1 = (Button) findViewById(R.id.edm_beat_1);
                    beat_1.setBackgroundResource(android.R.drawable.btn_default);
                    Button beat_2 = (Button) findViewById(R.id.edm_beat_2);
                    beat_2.setBackgroundResource(android.R.drawable.btn_default);
                    beat_1_playing = 0;
                    beat_2_playing = 0;

                }

                if(beat_3_playing==0) {
                    switch(tempo) {
                        case 60:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 0.6f);
                            break;
                        case 70:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 0.7f);
                            break;
                        case 80:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 0.8f);
                            break;
                        case 90:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 0.9f);
                            break;
                        case 100:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 1.0f);
                            break;
                        case 110:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 1.1f);
                            break;
                        case 120:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 1.2f);
                            break;
                        case 130:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 1.3f);
                            break;
                        case 140:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 1.4f);
                            break;
                        case 150:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 1.5f);
                            break;
                        case 160:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 1.6f);
                            break;
                        case 170:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 1.7f);
                            break;
                        case 180:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 1.8f);
                            break;
                        case 190:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 1.9f);
                            break;
                        case 200:
                            currently_playing_beat_ID = edmCompositionSoundPoolBackground.play(beat_3_100, 0.7f, 0.7f, 1, -1, 2.0f);
                            break;
                    }

                    view.setBackgroundColor(Color.BLACK);
                    beat_3_playing = 1;


                }
                else {
                    edmCompositionSoundPoolBackground.stop(currently_playing_beat_ID);
                    view.setBackgroundResource(android.R.drawable.btn_default);
                    beat_3_playing = 0;
                }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();

    }
/*
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        for(int i = 0; i < numberOfKeys; i++)
        key[i].release();
    }
*/
}
